//
//  SLChatBoxDefault.h
//  MatchMaker
//
//  Created by mac on 2017/12/4.
//  Copyright © 2017年 janice. All rights reserved.
//

#ifndef SLChatBoxDefault_h
#define SLChatBoxDefault_h

#define WeakSelf(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define Screen_W                [UIScreen mainScreen].bounds.size.width
#define Screen_H                [UIScreen mainScreen].bounds.size.height
#define kScale                  Screen_W/750.0

#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

#define Font_Bold(font)         [UIFont fontWithName:@"PingFangSC-Medium" size:font]?[UIFont fontWithName:@"PingFangSC-Medium" size:font]:[UIFont systemFontOfSize:font]
#define Font_Medium(font)       [UIFont fontWithName:@"PingFangSC-Regular" size:font]?[UIFont fontWithName:@"PingFangSC-Regular" size:font]:[UIFont systemFontOfSize:font]
#define Font_Regular(font)      [UIFont fontWithName:@"PingFangSC-Light" size:font]?[UIFont fontWithName:@"PingFangSC-Light" size:font]:[UIFont systemFontOfSize:font]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//iPhoneX适配
#define ZPisIPhoneX       ([[UIScreen mainScreen] bounds].size.height == 812)
#define ZPTabBarHeight    (ZPisIPhoneX ? 83 : 49)
#define ZPStatutesBarH    (ZPisIPhoneX ? 44 : 20)
#define ZPNavgationBarH   44
#define ZPTabBarH         49
#define ZPKSafeBottomH      (ZPisIPhoneX ? 34 : 0)
#define ZPKNavBarH          (ZPKSafeBottomH + 44)
#define ZPTopNavrH        (44 + ZPStatutesBarH)


#define ResourcePath(path)  [[NSBundle mainBundle] pathForResource:path ofType:@"png"]
#define ImageWithPath(path) [UIImage imageWithContentsOfFile:path]


#define  HEIGHT_TABBAR     110*kScale      // 就是chatBox的高度49
#define  CHATBOX_BUTTON_WIDTH    71*kScale    //37

#define  HEIGHT_TEXTVIEW   70*kScale  //36


#define  BOX_TEXTView_SPACE  19*kScale      //键盘盒子6.5
#define  BOX_BTN_SPACE 4
#define  BOX_OTHERH   260*kScale                       //其它215
#define  BOX_OTHERH_FACE   430*kScale                       //其它215
#define  BOX_TOTALH   (HEIGHT_TABBAR+BOXOTHERH) //盒子的总高
#define  BOX_BTN_BOTTOM_SPACE    6              //盒子下部的间距

#import "MainShareView.h"

#endif /* SLChatBoxDefault_h */

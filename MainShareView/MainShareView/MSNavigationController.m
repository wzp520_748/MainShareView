//
//  MSNavigationController.m
//  MainShareView
//
//  Created by mac on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "MSNavigationController.h"

@interface MSNavigationController ()

@end

@implementation MSNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
        [[MainShareView mainShareView] setfFrame:CGRectMake(0, Screen_H-90*kScale-25*kScale, Screen_W, 90*kScale)];
    }
    return [super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated{
    [[MainShareView mainShareView] setfFrame:CGRectMake(0, Screen_H-ZPTabBarHeight-90*kScale-25*kScale, Screen_W, 90*kScale)];
    return [super popViewControllerAnimated:YES];
}

@end

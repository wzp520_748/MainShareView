//
//  HomeCell_CollectionView.h
//  MatchMaker
//
//  Created by mac on 2018/3/16.
//  Copyright © 2018年 janice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCell_CollectionView : UITableViewCell
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, copy) void(^didSelectItemAtIndexPaths)(NSIndexPath *indexPaths);

+ (instancetype)cellWithTableView:(UITableView *)tableView;
- (void)setupRelaod:(NSArray *)array;
@end

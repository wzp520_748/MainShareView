//
//  HomesCell_CollectionViewCell.h
//  MatchMaker
//
//  Created by mac on 2018/3/16.
//  Copyright © 2018年 janice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomesCell_CollectionViewCell : UICollectionViewCell
@property (nonatomic, retain) UIImageView *headImg;
@property (nonatomic, retain) UILabel *nicknameLabel;
@property (nonatomic, retain) UILabel *descrip;

@end

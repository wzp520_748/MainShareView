//
//  MainShareView.m
//  MatchMaker
//
//  Created by mac on 2018/3/19.
//  Copyright © 2018年 janice. All rights reserved.
//

#import "MainShareView.h"

@interface MainShareView()<NSCopying,NSMutableCopying>

@end

@implementation MainShareView

static MainShareView *_mainShareView;

+ (instancetype)mainShareView{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _mainShareView = [[self alloc] initWithFrame:CGRectMake(0, Screen_H-ZPTabBarHeight-90*kScale-25*kScale, Screen_W, 80*kScale)];
    });
    return _mainShareView;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _mainShareView = [super allocWithZone:zone];
    });
    return _mainShareView;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.frame = CGRectMake(0, Screen_H-ZPTabBarHeight-90*kScale-25*kScale, Screen_W, 90*kScale);
        [self setupUI];
    }
    return self;
}


- (id)copyWithZone:(nullable NSZone *)zone{
    return _mainShareView;
}

- (id)mutableCopyWithZone:(nullable NSZone *)zone{
    return _mainShareView;
}

- (void)setupHiddenFrame:(BOOL)isHidden{
//    CGRect current = _mainShareView.frame;
    [UIView animateWithDuration:0.7 animations:^{
        if (isHidden) {
            _mainShareView.bounds = CGRectMake(0, 0, Screen_W, 0);
            _mainShareView.frame = CGRectMake(0, Screen_H, Screen_W, 0);
        }else{
            _mainShareView.bounds = CGRectMake(0, 0, Screen_W, 0);
            _mainShareView.frame = CGRectMake(0, Screen_H-ZPTabBarHeight, Screen_W, 0);
        }
    }];
}

- (void)setfFrame:(CGRect)frame{
    [UIView animateWithDuration:0.7 animations:^{
        _mainShareView.frame = frame;
    }];
}

- (void)setupShowFrame:(BOOL)isHidden{
    [UIView animateWithDuration:0.7 animations:^{
        if (isHidden) {
            _mainShareView.bounds = CGRectMake(0, 0, Screen_W, 90*kScale);
            _mainShareView.frame = CGRectMake(0, Screen_H-90*kScale-25*kScale, Screen_W, 90*kScale);
        }else{
            _mainShareView.bounds = CGRectMake(0, 0, Screen_W, 90*kScale);
            _mainShareView.frame = CGRectMake(0, Screen_H-ZPTabBarHeight-90*kScale-25*kScale, Screen_W, 90*kScale);
        }
    }];
}

- (void)setupUI{
    
    UIView *homeView = [[UIView alloc] initWithFrame:CGRectMake(25*kScale, 0, Screen_W-50*kScale, 90*kScale)];
    homeView.layer.cornerRadius = 10*kScale;
    homeView.layer.masksToBounds = YES;
    homeView.backgroundColor = [UIColor grayColor];
    [self addSubview:homeView];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15*kScale, 10*kScale, 70*kScale, 70*kScale)];
    _imageView.layer.cornerRadius = 4*kScale;
    _imageView.layer.masksToBounds = YES;
    [homeView addSubview:_imageView];
    _imageView.image = [UIImage imageNamed:@"logo_log"];
    
    _titleL = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageView.frame) + 25*kScale, 10*kScale, 350*kScale, 35*kScale)];
    _titleL.font = Font_Bold(23*kScale);
    _titleL.textColor = [UIColor whiteColor];
    _titleL.text = @"十多个跟你说个事本地";
    [homeView addSubview:_titleL];
    
    _contentL = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageView.frame) + 25*kScale, CGRectGetMaxY(_titleL.frame)+5*kScale, 350*kScale, 20*kScale)];
    _contentL.font = Font_Bold(19*kScale);
    _contentL.textColor = [UIColor whiteColor];
    _contentL.text = @"09:54 - 逻辑思维";
    [homeView addSubview:_contentL];
    
    self.startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.startBtn.frame = CGRectMake(homeView.frame.size.width - 80*kScale, 20*kScale, 50*kScale, 50*kScale);
    [self.startBtn setBackgroundImage:[UIImage imageNamed:@"logo_log"] forState:UIControlStateNormal];
    [homeView addSubview:self.startBtn];
    [self.startBtn addTarget:self action:@selector(actionStartBtn:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)actionStartBtn:(UIButton *)sender{
    NSLog(@"播放");
}

@end

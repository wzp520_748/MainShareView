//
//  HomeViewController.m
//  MainShareView
//
//  Created by mac on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "HomeViewController.h"
#import "HomesViewControllers.h"
#import "HomeCell_CollectionView.h"

@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    CGFloat lastContentOffset;
}
@property (strong, nonatomic) UITableView *tableView;
@end

@implementation HomeViewController
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationController.navigationBar.frame), Screen_W, Screen_H-64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.view addSubview:_tableView];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = 160*kScale;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.backgroundColor = RGBA(249, 249, 249, 1);
    }
    return _tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"主页";
    self.view.backgroundColor = [UIColor whiteColor];
    [self tableView];
    [self getStarAndFriData];
}

#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 400*kScale;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeCell_CollectionView *cell = [HomeCell_CollectionView cellWithTableView:tableView];
    
    cell.didSelectItemAtIndexPaths = ^(NSIndexPath *indexPaths) {
        HomesViewControllers *vc = [[HomesViewControllers alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    };
    [cell setupRelaod:nil];
    return cell;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    lastContentOffset = scrollView.contentOffset.y;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y<lastContentOffset){
        //向上
        [[MainShareView mainShareView] setupHiddenFrame:self.tabBarController.tabBar.isHidden];
    } else if (scrollView.contentOffset.y>lastContentOffset){
        //向下
        [[MainShareView mainShareView] setupShowFrame:self.tabBarController.tabBar.isHidden];
    }
    
}

- (void)getStarAndFriData {
    //延迟2秒
    double delaySeconds = 2;
    //创建时间
    dispatch_time_t delay_time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
    //执行延迟任务
    dispatch_after(delay_time, dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

@end

//
//  MainShareView.h
//  MatchMaker
//
//  Created by mac on 2018/3/19.
//  Copyright © 2018年 janice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainShareView : UIView
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UILabel *contentL;
@property (nonatomic, strong) UIButton *startBtn;
+ (instancetype)mainShareView;
- (void)setupHiddenFrame:(BOOL)isHidden;
- (void)setupShowFrame:(BOOL)isHidden;
- (void)setfFrame:(CGRect)frame;
@end

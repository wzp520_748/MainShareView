//
//  MainTabBarController.m
//  MainShareView
//
//  Created by mac on 2018/3/19.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "MainTabBarController.h"
#import "HomeViewController.h"
#import "MSNavigationController.h"

@interface MainTabBarController ()

@end

@implementation MainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HomeViewController *homeVC = [[HomeViewController alloc] init];
    MSNavigationController * homeNav = [self setNavigationController:homeVC title:@"主页" image:[UIImage imageNamed:@"icon_home"] selectImage:[UIImage imageNamed:@"icon_home"] tag:0];
    self.viewControllers = @[homeNav];
}

- (MSNavigationController *)setNavigationController:(UIViewController *)vc title:(NSString *)title image:(UIImage *)image selectImage:(UIImage *)selectImage tag:(NSInteger)tag {
    MSNavigationController * nav = [[MSNavigationController alloc] initWithRootViewController:vc];
    nav.tabBarItem = [[UITabBarItem alloc]
                      initWithTitle:title
                      image:image
                      selectedImage:[selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [nav.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0x333333), NSFontAttributeName : [UIFont systemFontOfSize:10 weight:UIFontWeightSemibold]} forState:UIControlStateNormal];
    [nav.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0x04C4C9), NSFontAttributeName : [UIFont systemFontOfSize:10 weight:UIFontWeightSemibold]} forState:UIControlStateHighlighted];
    nav.tabBarItem.tag = tag;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nav.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    }
    
    return nav;
}

@end
